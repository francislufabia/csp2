console.log("hello from JS file");

let createCourse = document.querySelector("#createCourse");

createCourse.addEventListener("submit", (event) => {
	event.preventDefault();
	let courseName = document.querySelector("#courseName").value
	let coursePrice = document.querySelector("#coursePrice").value
	let courseDescription = document.querySelector("#courseDescription").value	

if(courseName == "" || coursePrice == "" || courseDescription ==""){
	Swal.fire ({
		icon: 'error',
		title: 'No blanks',
		text: 'Admins, please put proper details of the course'		
	})
	// alert("No blanks!")
	}else {
		fetch('https://pure-beach-95377.herokuapp.com/api/courses/course-exists', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body:JSON.stringify({
			name: courseName,
		})
	}).then(res => res.json()
		)//NORMAL POSTING METHOD
		.then(data => {
			if(data === false){
				fetch("https://pure-beach-95377.herokuapp.com/api/courses/addCourse", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: courseName,
					description: courseDescription,
					price: coursePrice
				})
			}).then(res => {
				return res.json()
			}).then(data => {
				if(data === true) {
					Swal.fire({
						icon: 'success',
						title: 'Course created',
					})
					// alert("New course created!")
				} else {
					alert("Error creating the course.")
				}
			})
			}else {
				Swal.fire ({
					icon: 'error',
					title: 'Same Course Name',
					text: 'Admins, please choose a different name'		
				})
				// alert("Same coursename, choose a different name")
			}
		})
	}
})
	