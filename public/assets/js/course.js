console.log("hello from JS")

//idenfity which course it needs to display inside the browser?
//to do this, we are going to use the course ID to identify the correct course properl

let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the "current" location of the doucment
//.search contains the query string section of the current URL
// URLSearchParams() - this method/constructor creates and returns a URLSearchParams object.
// URLSearchParams describes the interface that defines utility methods to work with the query string of a URL.
// new -> instantiates a user-defined object.

let id = params.get('courseId')
console.log(id)

//lets capture the access token from the local storage (for the enroll button)
let token = localStorage.getItem('token')
console.log(token)

//lets capture the sections of the HTML bodies
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")

fetch(`https://pure-beach-95377.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)

	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-danger text-white btn-block">Enroll</a>`
	//lines below will insert the enroll button component inside this page.
	//we have to capture first the anchor tag for enroll (document.selector below), then add an event listener to trigger an event. then create a function in the eventlistener to describe the next se of procedure
	document.querySelector("#enrollButton").addEventListener("click",() => {
		fetch('https://pure-beach-95377.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},//token above is from the previous line token variable.
			body: JSON.stringify({
				courseId: id
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				Swal.fire({
					icon: 'success',
					title: 'Sucessfully Enrolled',
				})				//redirect
				// window.location.replace("./courses.html")
			}else{
				//inform the user that the request has failed
				Swal.fire({
					icon: 'error',
					title: 'You are not enrolled',
				})	
			}
		})	
	})
})

